package com.c2t.spring.hbm;

import org.hibernate.Session;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppMain 
{
    public static void main( String[] args )
    {
    	ApplicationContext appContext = 
    		new ClassPathXmlApplicationContext("spring/config/BeanLocations-sf.xml");
	
    	org.hibernate.impl.SessionFactoryImpl sf = (org.hibernate.impl.SessionFactoryImpl)appContext.getBean("sessionFactory1");
    	Session session = sf.openSession();
    	session.beginTransaction();
    	
    	
    }
}
