package com.c2t.spring.hbm.copy;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App3 
{
    public static void main( String[] args )
    {
    	ApplicationContext appContext = 
    		new ClassPathXmlApplicationContext("spring/config/BeanLocations-sf.xml");
	
    	SpringHibernateSessionFactory stockDao = (SpringHibernateSessionFactory)appContext.getBean("springHibernate");
    	
    	/** insert **/
    	Stock stock = new Stock();
    	stock.setStockCode("7668");
    	stock.setStockName("HAIO");
    	stockDao.save(stock);
    	
    	/** select **/
    	/*Stock stock2 = stockDao.findByStockCode("7668");
    	System.out.println(stock2);*/
    	
    	/** update **/
    	/*stock2.setStockName("HAIO-1");
    	stockDao.update(stock2);*/
    	
    	/** delete **/
    	//stockBo.delete(stock2);
    	
    	System.out.println("Done");
    }
}
