package com.c2t.spring.hbm.copy;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.orm.toplink.SessionFactory;

public class StockDaoImpl extends HibernateDaoSupport implements StockDao{
	
	public void save(Stock stock){
		getHibernateTemplate().save(stock);
		/*SessionFactory sf  = getSessionFactory();
		Session session = sf.createSession();
		
		session.beginTransaction();
		
		session.save(stock);
		
		session.getTransaction().commit();*/
	}
	
	public void update(Stock stock){
		//getHibernateTemplate().update(stock);
	}
	
	public void delete(Stock stock){
		//getHibernateTemplate().delete(stock);
	}
	
	public Stock findByStockCode(String stockCode){
		List list = getHibernateTemplate().find("from Stock where stockCode=?",stockCode);
		return (Stock)list.get(0);
	}

}
