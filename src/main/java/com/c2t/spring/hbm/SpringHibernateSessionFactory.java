package com.c2t.spring.hbm;

import org.hibernate.Session;
import org.hibernate.impl.SessionFactoryImpl;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;

public class SpringHibernateSessionFactory {

	org.hibernate.impl.SessionFactoryImpl sessionFactory;

	SpringHibernateSessionFactory() {

	}

	public SessionFactoryImpl getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactoryImpl sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void save(Stock stock) {

		Session session = sessionFactory.openSession();

		session.beginTransaction();

		session.save(stock);
		session.getTransaction().commit();

	}

}
